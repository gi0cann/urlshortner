class CreateLinks < ActiveRecord::Migration
  def change
    create_table :links do |t|
      t.string :url
      t.string :url_hash

      t.timestamps
    end
  end
end
