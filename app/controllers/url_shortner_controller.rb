class UrlShortnerController < ApplicationController
  def urlform
  end

  def retrieve
    hash = params[:hash]
    url = Link.find_by(url_hash: hash)
    if url == nil
      @result = "Sorry url not found. Please check you url and try again or generate a new url"
    else
      redirect_to url.url
    end
  end

  def urlresult
    url = params[:url_shortner][:url]
    hash = rand(36**7).to_s(36)
    while true
      check_hash = Link.find_by(url_hash: hash)
      if check_hash == nil
        link = Link.create(url: url, url_hash: hash)
        break
      end
    end
    @result = "#{request.headers['Host']}/#{hash}"
  end
end
