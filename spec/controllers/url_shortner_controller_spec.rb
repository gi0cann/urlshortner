require 'rails_helper'

RSpec.describe UrlShortnerController, :type => :controller do

  describe "GET urlform" do
    it "returns http success" do
      get :urlform
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET retrieve" do
    it "returns http success" do
      get :retrieve
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET urlresult" do
    it "returns http success" do
      get :urlresult
      expect(response).to have_http_status(:success)
    end
  end

end
